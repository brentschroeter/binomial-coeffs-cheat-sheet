\documentclass[10pt]{article} % Default font size

\title{Knuth's Binomial Coefficients Cheat Sheet}
\author{Brent Schroeter}

\usepackage[margin=1in]{geometry} % Use A4 paper and set margins
\usepackage{amsmath}

\pagenumbering{gobble}
\setcounter{secnumdepth}{0} % Suppress section numbering
\setlength{\parindent}{0pt}

\begin{document}

\begin{center}
    {\LARGE Knuth's Binomial Coefficients Cheat Sheet}

    \vspace{2em}

    Equations from Section 1.2.6 of {\it The Art of Computer Programming, Vol. 1, 2nd Edition}.

    Compiled by Brent Schroeter.
\end{center}

\vspace{3em}

\begin{minipage}{\textwidth}
    Definition for Integers

    \begin{equation} \tag{2}
    \begin{aligned}
        \binom{n}{k} &= \frac{n (n-1) \cdots (n-k+1)}{k (k-1) \cdots (1)},
            & \qquad\text{integer } n \geq 0, \quad \text{integer } k \geq 0
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Generalization to Reals

    \begin{equation} \tag{3}
    \begin{aligned}
        \binom{r}{k} = \frac{r (r-1) \cdots (r-k+1)}{k (k-1) \cdots (1)} &=
            \prod_{1 \leq j \leq k} \left(\frac{r+1-j}{j}\right),
            & \qquad\text{integer } k \geq 0; \\
        \binom{r}{k} &= 0,
            & \qquad\text{integer } k < 0.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Particular Cases

    \begin{equation} \tag{4}
    \begin{aligned}
        \binom{r}{0} = 1, \qquad \binom{r}{1} = r, \qquad \binom{r}{2} = \frac{r (r-1)}{2}.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Representation by Factorials

    \begin{equation} \tag{5}
    \begin{aligned}
        \binom{n}{k} &= \frac{n!}{k! (n-k)!},
            & \qquad\text{integer } n \geq \text{integer } k \geq 0.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Symmetry Condition

    \begin{equation} \tag{6}
    \begin{aligned}
        \binom{n}{k} &= \binom{n}{n-k},
            & \qquad\text{integer } n \geq 0, \text{integer } k.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Moving In and Out of Brackets

    \begin{equation} \tag{7}
    \begin{aligned}
        \binom{r}{k} &= \frac{r}{k} \binom{r-1}{k-1},
            & \qquad\text{integer } k \neq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{8}
    \begin{aligned}
        \binom{r}{k} &= \frac{r}{r-k} \binom{r-1}{k},
            & \qquad\text{integer } k \neq r.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Addition Formula

    \begin{equation} \tag{9}
    \begin{aligned}
        \binom{r}{k} &= \binom{r-1}{k} + \binom{r-1}{k-1},
            & \qquad\text{integer } k.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}

    Summation Formula

    \begin{equation} \tag{10}
    \begin{aligned}
        \sum_{0 \leq k \leq n} \binom{r+k}{k} &=
            \binom{r}{0} + \binom{r+1}{1} + \cdots + \binom{r+n}{n} = \binom{r+n+1}{n},
            & \qquad\text{integer } n \geq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{11}
    \begin{aligned}
        \sum_{0 \leq k \leq n} \binom{k}{m} &=
            \binom{0}{m} + \binom{1}{m} + \cdots + \binom{n}{m} = \binom{n+1}{m+1},
            & \qquad\text{integer } m \geq 0, \quad \text{integer } n \geq 0.
    \end{aligned}
    \end{equation}

\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    The Binomial Theorem

    \begin{equation} \tag{13}
    \begin{aligned}
        {(x+y)}^r &= \sum_k \binom{r}{k} x^k y^{r-k},
            & \qquad\text{integer } r \geq 0.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Special Case of the Binomial Theorem

    \begin{equation} \tag{15}
    \begin{aligned}
        \sum_k \binom{r}{k} x^k &= {(1+x)}^r,
            & \qquad\text{integer } r \geq 0, \quad \text{or} \quad |x| < 1.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Abel's Generalization

    \begin{equation} \tag{16}
    \begin{aligned}
        {(x+y)}^r &= \sum_k \binom{r}{k} x {(x-kz)}^{k-1} {(y+kz)}^{r-k},
            & \qquad\text{integer } r \geq 0, \quad x \neq 0.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Negating the Upper Index

    \begin{equation} \tag{17}
    \begin{aligned}
        \binom{-r}{k} &= {(-1)}^k \binom{r + k - 1}{k},
            & \qquad\text{integer } k.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Negating the Upper Index When $r$ Is an Integer

    \begin{equation} \tag{19}
    \begin{aligned}
        \binom{n}{m} &= {(-1)}^{n-m} \binom{-(m+1)}{n-m},
            & \qquad\text{integer } n \geq 0, \quad \text{integer } m.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Simplifying Products

    \begin{equation} \tag{20}
    \begin{aligned}
        \binom{r}{m} \binom{m}{k} &= \binom{r}{k} \binom{r-k}{m-k},
            & \qquad\text{integer } m, \quad \text{integer } k.
    \end{aligned}
    \end{equation}
\end{minipage}

\vspace{2em}

\begin{minipage}{\textwidth}
    Sums of Products

    \begin{equation} \tag{21}
    \begin{aligned}
        \sum_k \binom{r}{k} \binom{s}{n-k} &= \binom{r+s}{n},
            & \qquad\text{integer } n.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{22}
    \begin{aligned}
        \sum_k \binom{r}{k} \binom{s}{n+k} &= \binom{r+s}{r+n},
            & \qquad\text{integer } n, \quad \text{integer } r \geq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{23}
    \begin{aligned}
        \sum_k \binom{r}{k} \binom{s+k}{n} {(-1)}^k &= {(-1)}^r \binom{s}{n-r},
            & \qquad\text{integer } n, \quad \text{integer } r \geq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{24}
    \begin{aligned}
        \sum_{0 \leq k \leq r} \binom{r-k}{m} \binom{s}{k-t} {(-1)}^k &=
            {(-1)}^t \binom{r-t-s}{r-t-m},\\
            & \qquad \text{integer } t \geq 0, \quad \text{integer } r \geq 0,
            \quad \text{integer } m \geq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{25}
    \begin{aligned}
        \sum_{0 \leq k \leq r} \binom{r-k}{m} \binom{s + k}{n} &= \binom{r+s+1}{m+n+1}, \\
            & \qquad\text{integer } n \geq 0, \quad \text{integer } s \geq 0,
            \quad \text{integer } m \geq 0, \quad \text{integer } r \geq 0.
    \end{aligned}
    \end{equation}

    \vspace{1em}

    \begin{equation} \tag{26}
    \begin{aligned}
        \sum_{k \geq 0} \binom{r-tk}{k} \binom{s-t(n-k)}{n-k} \frac{r}{r-tk}
            &= \binom{r+s-tn}{n},
            & \qquad\text{integer } n.
    \end{aligned}
    \end{equation}
\end{minipage}

\end{document}
